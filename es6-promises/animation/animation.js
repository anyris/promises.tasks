(function () {

let secretElement = document.getElementById('secret');
let openedElement = document.getElementById('opened');

let runTimeoutAnimation = () => {	
	delay(1000)
	.then(() => {
			openedElement.classList.add('element-animation');
			return delay(1000);
		}		
	)
	.then(() =>{
			secretElement.classList.add('hide-animation');
			return delay(1000);	
		}		
	)
	.then(() => openedElement.classList.add('size-animation'));
};
function delay(interval) {
    return new Promise(resolve => setTimeout(resolve, interval))
   
}

runTimeoutAnimation();

})();

/*
CSS3 анимация на промисах. Задача 'animation'
 - познакомиться с css3 animations: http://www.w3schools.com/css/css3_animations.asp и др.
 - для решения задачи необходимо обернуть setTimeout в Promise; пусть новая функция называется delay
 - delay возвращает promise, который резолвится (путем вызыва resolve-колбэка) по истечени таймаута
 - выстроить вместо бесконечновложенного утко-кода из setTimeout последовательную цепочку вызовов вида delay().then().then().then()
 - при запуске animation.html анимация на промисах должна отработать в точности так же, как и в исходном виде

setTimeout(() => {
		openedElement.classList.add('element-animation');
		setTimeout(() => {
			openedElement.classList.add('hide-animation');
			setTimeout(() => {
				secretElement.classList.add('size-animation');
			}, 2000);
		}, 1000);
	}, 1000);

	*/