(function () {

let notificationElement = document.getElementById('notification');

let postApiList = [
	'https://jsonplaceholder.typicode.com/posts/1',
	'https://jsonplaceholder.typicode.com/posts/2',
	'https://jsonplaceholder.typicode.com/posts/3'
];

let injectIndexedData = (index, data) => {
	let titleElement = document.getElementById('title' + index);
	let bodyElement = document.getElementById('post' + index);
	titleElement.innerHTML += ') ' + data.title;
	bodyElement.innerHTML = data.body;
};

let onLoadSuccess = () => {
	notificationElement.innerHTML = 'All API-requests are done.';
};

let promiseList = postApiList.map(url => fetch(url).then(res => res.json().then(item => injectIndexedData(item.id, item) )));
Promise.all(promiseList)
.then(response => onLoadSuccess())
.catch(error => console.log(error))



/*
Существует несколько вариантов имплементаци этой задачи:
 - в массив promiseList можно класть fetches с цепочкой then.then и тогда на Promise.all у нас будет голый вызов onLoadSuccess
 - можно класть fetches без then, тогда внутри Promise.all нужно будет устроить пробег по результатам с дополнительным then-резолвом json и вызовом injectIndexedData на каждом результате
 - комбинированный вариант: в promiseList кладутся fetches с одним then, резолвящим json, а внутри Promise.all просто синхронно пробегаются результаты и для каждого вызывается injectIndexedData
1)
let promiseList = postApiList.map(url => fetch(url).then(res => res.json().then(item => injectIndexedData(item.id, item) )));
Promise.all(promiseList)
.then(response => onLoadSuccess())
.catch(error => console.log(error))

2)
let promiseList = postApiList.map(url => fetch(url));
Promise.all(promiseList)
.then(response => {	
	response.forEach( (item, index) => item.json().then(res => injectIndexedData(res.id, res)) );	
	 onLoadSuccess();
})
.catch(error => console.log(error))

3)
let promiseList = postApiList.map(url => fetch(url).then(res => res.json()));

Promise.all(promiseList)
.then(response => {
	response.forEach((item, index) => injectIndexedData(item.id, item));
	onLoadSuccess();
})
.catch(error => console.log(error))

4)
 Promise.all(promises)
.then(response => {
	let results = [];
	for(let i = response.length - 1; i >= 0; i--) {
		results.push(response[i].json());
	}
	return Promise.all(results);
})
.then(results => {
	for(let i = results.length - 1; i >= 0; i--) {
		injectIndexedData(results[i].id, results[i]);
	}

	return new Promise(resolve => setTimeout(resolve, interval));
})
.then(result => onLoadSuccess())
.catch(error => console.log(error))
*/

})();